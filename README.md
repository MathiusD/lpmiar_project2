# Etude ce cas : le NIR

Le numéro NIR (Numéro d'Inscription au Répertoire) 
est le numéro d'inscription au répertoire national d'identification des personnes physiques, 
et il est identique au numéro de sécurité sociale. 
La gestion du numéro NIR a été confiée à l'INSEE, c'est pourquoi on parle parfois aussi du numéro INSEE.

Ce numéro sert d'identifiant unique pour chaque individu
 inscrit au RNIPP (Répertoire National d'Identification des Personnes Physiques).

Plus de précisions par sur [Wikipedia](https://fr.wikipedia.org/wiki/Num%C3%A9ro_de_s%C3%A9curit%C3%A9_sociale_en_France)


Nous allons considérer une version "simplifiée" du NIR. 
Dans notre cas, il s'agira d'un "numéro" à 13 chiffres 
+ une clef de contrôle à deux chiffres, définis comme suit : 

+ En position 1 : 1 pour les hommes, 2 pour les femmes
+ En positions 2 et 3 : deux derniers chiffres de l'année de naissance
+ En positions 4 et 5 : mois de naissance de 01 (janvier) à 12 (décembre) 
+ En positions 6 et 7 : code officiel du département de naissance 
+ En positions 8, 9 et 10 : code officiel de la commune de naissance de 001 à 999 
+ En positions 11, 12 et 13	: numéro d’ordre de la naissance (=, pour simplifier, ordre de fabrication du NIR) 
dans une commune, une année et un mois considéré

La clé de contrôle est le complément à 97 du nombre formé par les 13 chiffres du NIR modulo 97, c'est-à-dire
    `clef = 97 - (numero modulo 97)`.
    
Pour la corse, pour le calcul de la clé de contrôle, les lettres A et B sont à remplacer par des zéros,
 et on soustrait du nombre à 13 chiffres ainsi obtenu 1000000 pour A et 2000000 pour B.


# A faire 
1. coder (et tester) la méthode `controler()`
2. fabriquer un NIR correct
    1. coder la méthode `fabriquerCodeSexe()`
    2. coder la méthode `fabriquerCodeAnnee()`
    3. coder la méthode `fabriquerCodeMois()`
    4. coder la méthode `fabriquerCodeCommune()` en lisant le fichier `code-insee.csv`
    
    Pour lire un fichier `.csv`, vous pourrez (par exemple) utiliser la librairie [OpenCSV](http://opencsv.sourceforge.net/)
    normalement déjà installée (si jamais : IntelliJ > Project structure > Libraries > + > Maven > rechercher `com.opencsv:opencsv:5.2`).
    
    5. coder la méthode `fabriquerCodeDepartement()` qui utilise l'API https://geo.api.gouv.fr/, 
    avec par exemple la requête suivante : https://geo.api.gouv.fr/departements?nom=DOUBS&fields=code
    
    Pour exécuter une requête JSON, vous pourrez utiliser la librairie [Unirest-java](http://kong.github.io/unirest-java),
    normalement déjà installée (si jamais : IntelliJ > Project structure > Libraries > + > Maven > rechercher `com.konghq:unirest-java:3.10.00`).
    
    6. finalement, coder la méthode `fabriquerNIR()` en réutilisant les 
    méthodes codées précédemment

Etendre el code proposé pour prendre en compte les français nés à l'étranger (ajouter les méthodes, classes, webservices nécessaires) et développer également des cas de tests