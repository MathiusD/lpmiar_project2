import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestNIR {


    @Test
    public void test_controler1() {
        NIR nir = new NIR("2860725031001","12");
        assertTrue(nir.controler());
    }

    @Test
    public void test_controler12() {
        NIR nir = new NIR("2860725032001","12");
        assertFalse(nir.controler());
    }


    @Test
    public void test_controler2() {
        NIR nir = new NIR("1860701206001","19");
        assertTrue(nir.controler());
    }

    @Test
    public void test_controler22() {
        NIR nir = new NIR("1860701206001","18");
        assertFalse(nir.controler());
    }

    @Test
    public void test_controler3() {
        NIR nir = new NIR("186072B010001","08");
        assertTrue(nir.controler());
    }

    @Test
    public void test_controler33() {
        NIR nir = new NIR("286072B010001","08");
        assertFalse(nir.controler());
    }





    @Test
    public void test_sexe1() {
        assertEquals(2, NIR.fabriquerCodeSexe(Sexe.FEMME));
    }

    @Test
    public void test_sexe2() {
        assertEquals(1, NIR.fabriquerCodeSexe(Sexe.HOMME));
    }

    @Test
    public void test_mois() throws DateNaissanceException {
        assertEquals(7, NIR.FabriquerCodeMois("12/07/1986"));
    }

    @Test
    public void test_mois2() throws DateNaissanceException {
        assertEquals(11, NIR.FabriquerCodeMois("21/11/2004"));
    }

    @Test
    public void test_mois_ex1() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.FabriquerCodeMois("12/07/86"));
    }

    @Test
    public void test_mois_ex2() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.FabriquerCodeMois("12/13/1986"));
    }

    @Test
    public void test_mois_ex3() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.FabriquerCodeMois("32/07/1986"));
    }

    @Test
    public void test_annee() throws DateNaissanceException {
        assertEquals(86, NIR.fabriquerCodeAnnee("12/07/1986"));
    }

    @Test
    public void test_annee2() throws DateNaissanceException {
        assertEquals(4, NIR.fabriquerCodeAnnee("21/11/2004"));
    }

    @Test
    public void test_annee_ex1() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerCodeAnnee("12/07/86"));
    }

    @Test
    public void test_annee_ex2() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerCodeAnnee("12/13/1986"));
    }

    @Test
    public void test_annee_ex3() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerCodeAnnee("32/07/1986"));
    }

    @Test
    public void test_donnerCodeDepartement1() throws DepartementException {
        assertEquals("02", NIR.fabriquerCodeDepartement("AISNE"));
    }

    @Test
    public void test_donnerCodeDepartement2() throws DepartementException {
        assertEquals("25", NIR.fabriquerCodeDepartement("DOUBS"));
    }

    @Test
    public void test_donnerCodeDepartement25() throws DepartementException {
        assertEquals("25", NIR.fabriquerCodeDepartement("doubs"));
    }

    @Test
    public void test_donnerCodeDepartement3() throws DepartementException {
        assertEquals("79", NIR.fabriquerCodeDepartement("DEUX-SEVRES"));
    }


    @Test
    public void test_donnerCodeDepartement5() throws DepartementException {
        assertEquals("01", NIR.fabriquerCodeDepartement("AIN"));
    }

    @Test
    public void test_donnerCodeDepartement52() throws DepartementException {
        assertEquals("01", NIR.fabriquerCodeDepartement("Ain"));
    }

    @Test
    public void test_donnerCodeDepartement6() throws DepartementException {
        assertEquals("2A", NIR.fabriquerCodeDepartement("CORSE-DU-SUD"));
    }

    @Test
    public void test_donnerCodeDepartement78() throws DepartementException {
        assertEquals("2B", NIR.fabriquerCodeDepartement("HAUTE-CORSE"));
    }


    @Test
    public void test_donnerCodeDepartement_ex()  {
        assertThrows(DepartementException.class, ()-> NIR.fabriquerCodeDepartement(""));
    }

    @Test
    public void test_donnerCodeDepartement_ex2()  {
        assertThrows(DepartementException.class, ()-> NIR.fabriquerCodeDepartement("FRANCE"));
    }

    @Test
    public void test_donnerCodeDepartement_ex3()  {
        assertThrows(DepartementException.class, ()-> NIR.fabriquerCodeDepartement("CORSE"));
    }


    @Test
    public void test_donnerCodeCommune1() throws CommuneException {
        assertEquals("206", NIR.fabriquerCodeCommune("LANTENAY","AIN"));
    }

    @Test
    public void test_donnerCodeCommune2() throws CommuneException {
        assertEquals("010", NIR.fabriquerCodeCommune("ALGAJOLA",	"HAUTE-CORSE"));
    }

    @Test
    public void test_donnerCodeCommune3() throws CommuneException {
        assertEquals("008", NIR.fabriquerCodeCommune("ALBITRECCIA","CORSE-DU-SUD"));
    }

    @Test
    public void test_donnerCodeCommune4() throws CommuneException {
        assertEquals("030", NIR.fabriquerCodeCommune("BOUILLAC","AVEYRON"));
    }

    @Test
    public void test_donnerCodeCommune5() throws CommuneException {
        assertEquals("052", NIR.fabriquerCodeCommune("BOUILLAC","DORDOGNE"));
    }

    @Test
    public void test_donnerCodeCommune6() throws CommuneException {
        assertEquals("020", NIR.fabriquerCodeCommune("BOUILLAC","TARN-ET-GARONNE"));
    }

    @Test
    public void test_donnerCodeCommune_ex()  {
        assertThrows(CommuneException.class, ()-> NIR.fabriquerCodeCommune("CORSE" ,"CORSE-DU-SUD"));
    }

    @Test
    public void test_donnerCodeCommune_ex2() {
        assertThrows(CommuneException.class, ()-> NIR.fabriquerCodeCommune("ALBITRECCIA" ,"CORSE"));
    }

    @Test
    public void test_fabrique_ex1()  {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                "sdffdsf",
                "DOUBS",
                "AUDINCOURT"));
    }

    @Test
    public void test_fabrique_ex2() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                        "12/07/86",
                        "DOUBS",
                        "AUDINCOURT"));
    }

    @Test
    public void test_fabrique_ex3() {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                        "12/13/1986",
                        "DOUBS",
                        "AUDINCOURT"));
    }

    @Test
    public void test_fabrique_ex4()  {
        assertThrows(DateNaissanceException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                        "32/07/1986",
                        "DOUBS",
                        "AUDINCOURT"));
    }

    @Test
    public void test_fabrique_ex5()  {
        assertThrows(DepartementException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                        "28/07/1986",
                        "NANTES",
                        "PARIS"));
    }

    @Test
    public void test_fabrique_ex6()  {
        assertThrows(CommuneException.class,
                ()->NIR.fabriquerNIR(Sexe.FEMME,
                        "28/07/1986",
                        "DOUBS",
                        "PARIS"));
    }

    @Test
    public void test_fabrique() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir = NIR.fabriquerNIR(Sexe.FEMME,
                "12/07/1986",
                "DOUBS",
                "AUDINCOURT");
        assertEquals("2860725031001 12", nir.toString());
    }

    @Test
    public void test_fabrique2() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir = NIR.fabriquerNIR(Sexe.HOMME,
                "12/07/1986",
                "AIN",
                "LANTENAY");
        assertEquals("1860701206001 19", nir.toString());
    }

    @Test
    public void test_fabrique3() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir = NIR.fabriquerNIR(Sexe.HOMME,
                "12/07/1986",
                "HAUTE-CORSE",
                "ALGAJOLA");
        assertEquals("186072B010001 08", nir.toString());
    }

    @Test
    public void test_fabrique5() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir = NIR.fabriquerNIR(Sexe.HOMME,
                "24/12/2020",
                "CORSE-DU-SUD",
                "ALBITRECCIA");
        assertEquals("120122A008001 57", nir.toString());
    }



    @Test
    public void test_fabrique_plusieurs() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir1 = NIR.fabriquerNIR(Sexe.HOMME,
                "12/07/1988",
                "DOUBS",
                "AUDINCOURT");
        NIR nir2 = NIR.fabriquerNIR(Sexe.FEMME,
                "16/07/1988",
                "DOUBS",
                "AUDINCOURT");
        NIR nir3 = NIR.fabriquerNIR(Sexe.FEMME,
                "15/07/1988",
                "DOUBS",
                "AUDINCOURT");
        NIR nir4 = NIR.fabriquerNIR(Sexe.HOMME,
                "16/07/1988",
                "DOUBS",
                "AUDINCOURT");
        assertAll(
                ()-> assertTrue(nir1.toString().contains("880725031001")),
                ()-> assertTrue(nir2.toString().contains("880725031002")),
                ()-> assertTrue(nir3.toString().contains("880725031003")),
                ()-> assertTrue(nir4.toString().contains("880725031004"))
        );
    }

    @Test
    public void test_fabrique_plusieurs_plusieurs() throws DateNaissanceException, CommuneException, DepartementException {
        NIR nir1 = NIR.fabriquerNIR(Sexe.HOMME,
                "12/07/1992",
                "DOUBS",
                "AUDINCOURT");
        NIR nir2 = NIR.fabriquerNIR(Sexe.FEMME,
                "16/07/1992",
                "DOUBS",
                "AUDINCOURT");
        NIR autre_nir1 = NIR.fabriquerNIR(Sexe.FEMME,
                "16/07/2002",
                "DOUBS",
                "AUDINCOURT");
        NIR nir3 = NIR.fabriquerNIR(Sexe.FEMME,
                "15/07/1992",
                "DOUBS",
                "AUDINCOURT");
        NIR autre_nir2 = NIR.fabriquerNIR(Sexe.HOMME,
                "11/07/2002",
                "DOUBS",
                "AUDINCOURT");
        NIR nir4 = NIR.fabriquerNIR(Sexe.HOMME,
                "16/07/1992",
                "DOUBS",
                "AUDINCOURT");
        assertAll(
                ()-> assertTrue(nir1.toString().contains("920725031001")),
                ()-> assertTrue(nir2.toString().contains("920725031002")),
                ()-> assertTrue(nir3.toString().contains("920725031003")),
                ()-> assertTrue(nir4.toString().contains("920725031004")),
                ()-> assertTrue(autre_nir1.toString().contains("020725031001")),
                ()-> assertTrue(autre_nir2.toString().contains("020725031002"))
        );
    }


    @Test
    public void test_fabrique_beaucoup() throws DateNaissanceException, CommuneException, DepartementException {
        for (int i = 1; i <= 20 ; i++) {
            NIR.fabriquerNIR(Sexe.HOMME,
                    "12/09/2012",
                    "DOUBS",
                    "AUDINCOURT");
        }
        NIR nir = NIR.fabriquerNIR(Sexe.FEMME,
                "16/09/2012",
                "DOUBS",
                "AUDINCOURT");
         assertTrue(nir.toString().contains("120925031021"));
    }


}

