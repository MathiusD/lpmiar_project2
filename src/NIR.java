import com.google.gson.JsonObject;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


public class NIR {

    // attributs
    private String nir;
    private String clef;

    final private static int complement = 97;
    final private static String fileINSEE = "./code-insee.csv";
    final private static String api = "https://geo.api.gouv.fr/departements";
    private static HashMap<String, HashMap<Long, HashMap<Long, Long>>> saved = new HashMap<>();




    // constructeur
    public NIR(String nir, String clef) {
        this.nir = nir;
        this.clef = clef;
    }

    //
    @Override
    public String toString() {
        return nir + " " + clef;
    }


    private static Long extractNir(String nir) {
        String temp = nir;
        temp = nir.contains("A") ? temp.replace("A", "0") : (nir.contains("B") ? temp.replace("B", "0") : temp);
        Long out = Long.valueOf(temp);
        out -= 1000000 * (nir.contains("A") ? 1 : nir.contains("B") ? 2 : 0);
        return out;
    }

    private Long extractNir() {
        return NIR.extractNir(this.nir);
    }

    /**
     * controle le numero NIR vis-à-vis de sa clef
     * @return true, si <numero, clef> son cohérent
     */
    public boolean controler() {
        return Integer.valueOf(this.clef) == (NIR.complement -(this.extractNir() % NIR.complement)) ;
    }


    /**
     * donne le code sexe :  1 pour les hommes, 2 pour les femmes
     * @param sexe
     * @return
     */
    public static long fabriquerCodeSexe(Sexe sexe) {
        return sexe.equals(Sexe.HOMME) ? 1 : sexe.equals(Sexe.FEMME) ? 2 : null;
    }

    /**
     * donne les deux derniers chiffres de l'année de naissance
     * @param dateNaissance
     * @return
     * @throws DateNaissanceException si la date est incorrecte
     */
    public static long fabriquerCodeAnnee(String dateNaissance) throws DateNaissanceException {
        DateNaissance date = new DateNaissance(dateNaissance);
        return date.getShortAnnee();
    }

    /**
     * donne le mois de naissance de 01 (janvier) à 12 (décembre)
     * @param dateNaissance
     * @return
     * @throws DateNaissanceException si la date est incorrecte
     */
    public static long FabriquerCodeMois(String dateNaissance) throws DateNaissanceException {
        DateNaissance date = new DateNaissance(dateNaissance);
        return date.getMois();
    }

    /**
     * donne le code officiel de la commune de naissance (fichier code-insee.csv)
     * @param commune
     * @param departement
     * @return
     * @throws CommuneException si la commune est incorrecte
     */
    public static String fabriquerCodeCommune(String commune, String departement)
            throws  CommuneException {
        try {
            CSVReader reader = new CSVReader(new FileReader(NIR.fileINSEE));
            List<String[]> data =  reader.readAll();
            String out = null;
            for (String[] strings : data)
                if (strings[0].compareToIgnoreCase(commune) == 0 && strings[1].compareToIgnoreCase(departement) == 0 && out == null)
                    out = strings[2];
            if (out != null) {
                if (out.length() < 3)
                    if (out.length() < 2)
                        out = String.format("00%s", out);
                    else
                        out = String.format("0%s", out);
                return out;
            }
            else
                throw new CommuneException();
        } catch (IOException | CsvException e) {
            throw new CommuneException();
        }
    }

    /**
     * donne le code officiel du département de naissance
     * à partir d'une requete vers https://geo.api.gouv.fr/departements?nom=<departement>&fields=code
     * @param departement
     * @return
     * @throws DepartementException si le département est incorrect
     */
    public static String fabriquerCodeDepartement(String departement)
            throws  DepartementException {
        HttpResponse<String> response =  Unirest.get(String.format("%s?nom=%s", NIR.api, departement)).asString();
        if (response.getStatus() != 200)
            throw new DepartementException();
        JSONArray data = new JsonNode(response.getBody()).getArray();
        if (data.length() != 1)
            throw new DepartementException();
        JSONObject obj = data.getJSONObject(0);
        if (!obj.keySet().contains("code"))
            throw new DepartementException();
        return obj.getString("code");
    }

    /**
     * fabrique un NIR à partir des différents paramètres
     *
     * NB : utilisez les méthodes précédentes
     *
     * @param sexe
     * @param dateNaissance
     * @param departement
     * @param commune
     * @return un NIR
     * @throws DateNaissanceException si la date est incorrecte
     * @throws DepartementException si le département est incorrect
     * @throws CommuneException si la commune est incorrecte
     */
    public static NIR fabriquerNIR(Sexe sexe,
                                   String dateNaissance,
                                   String departement, String commune)
            throws DateNaissanceException, DepartementException, CommuneException {
        Long codeSexe = NIR.fabriquerCodeSexe(sexe);
        Long codeAnnee = NIR.fabriquerCodeAnnee(dateNaissance);
        String codeAnneeStr = String.valueOf(codeAnnee);
        if (codeAnneeStr.length() != 2)
            codeAnneeStr = String.format("0%d", codeAnnee);
        Long codeMois = NIR.FabriquerCodeMois(dateNaissance);
        String codeMoisStr = String.valueOf(codeMois);
        if (codeMoisStr.length() != 2)
                codeMoisStr = String.format("0%d", codeMois);
        String codeDepartement = NIR.fabriquerCodeDepartement(departement);
        String codeCommune = NIR.fabriquerCodeCommune(commune, departement);
        String nir = String.format(
                "%d%s%s%s%s",
                codeSexe,
                codeAnneeStr,
                codeMoisStr,
                codeDepartement,
                codeCommune
        );
        NIR.saved.putIfAbsent(codeCommune, new HashMap<>());
        NIR.saved.get(codeCommune).putIfAbsent(codeAnnee, new HashMap<>());
        NIR.saved.get(codeCommune).get(codeAnnee).putIfAbsent(codeMois, 0L);
        Long newIndice = NIR.saved.get(codeCommune).get(codeAnnee).get(codeMois) + 1;
        NIR.saved.get(codeCommune).get(codeAnnee).replace(codeMois, newIndice);
        String newIndiceStr = String.valueOf(newIndice);
        if (newIndiceStr.length() < 3)
            if (newIndiceStr.length() < 2)
                newIndiceStr = String.format("00%d", newIndice);
            else
                newIndiceStr = String.format("0%d", newIndice);
        nir = String.format("%s%s", nir, newIndiceStr);
        String clef = String.format("%d", NIR.complement - (NIR.extractNir(nir) % NIR.complement));
        if (clef.length() != 2)
            clef = String.format("0%s", clef);
        return new NIR(nir, clef);
    }

}


