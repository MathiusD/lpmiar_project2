public class DateNaissance {

    private int[] data;

    DateNaissance(String date) throws DateNaissanceException{
        String[] data = date.split("/");
        if (data.length == 3 && date.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")) {
            this.data = new int[3];
            for (int compt = 0; compt < data.length; compt++) {
                this.data[compt] = Integer.valueOf(data[compt]);
            }
            if (this.data[1] > 12)
                throw new DateNaissanceException();
            if (this.data[0] > 31)
                throw new DateNaissanceException();
        } else {
            throw new DateNaissanceException();
        }
    }

    public int getAnnee(){
        return this.data[2];
    }

    public int getMois(){
        return this.data[1];
    }

    public int getJour(){
        return this.data[0];
    }

    public int getShortAnnee(){
        return this.data[2] % 100;
    }
}
